import { Component, OnInit } from '@angular/core';
import { CommonService } from './common.service';
import { RenderValue, SortValue, TicketList } from 'src/app/interface';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private _commonService: CommonService) { }

  ticketList: TicketList[] = [

    { Name: "CustomerName", Order: SortValue.Ascending },
    { Name: "ContractNumber", Order: SortValue.Ascending },
    { Name: "CustomerId", Order: SortValue.Ascending },
    { Name: "TicketChannelName", Order: SortValue.Ascending },
    { Name: "ContactNo", Order: SortValue.Ascending },
    { Name: "Email", Order: SortValue.Ascending },
    { Name: "TicketTypeName", Order: SortValue.Ascending },
    { Name: "TicketAssignGroupName", Order: SortValue.Ascending },
    { Name: "AssignedTo", Order: SortValue.Ascending },
    { Name: "PriorityName", Order: SortValue.Ascending },
    { Name: "GroupName", Order: SortValue.Ascending },
    { Name: "SubGroupName", Order: SortValue.Ascending },
    { Name: "Subject", Order: SortValue.Ascending },
    { Name: "Description", Order: SortValue.Ascending }

  ]
  renderValueList: RenderValue[] = [];
  renderValueDuplicate: RenderValue[] = [];


  sort(item: TicketList) {
    debugger


    if (item.Order == SortValue.Ascending) {
      item.Order = SortValue.Descending
    } else { item.Order = SortValue.Ascending };




    this.renderValueList = this.renderValueList.sort(function (x: RenderValue, y: RenderValue) {
      debugger
      var sortValue = x[item.Name];
      var sortValueName = y[item.Name];
      let comparison = 0;
      if (sortValue > sortValueName) {
        if (item.Order == SortValue.Ascending) { comparison = 1 }
        else { comparison = -1 }
      } else if (sortValue < sortValueName) {
        if (item.Order == SortValue.Ascending) { comparison = -1 }
      } else { comparison = 1 }


      return comparison;
    })

  }

  pageSize = 2;

  ngOnInit() {

    this._commonService.renderGrid().subscribe(data => {
      this.renderValueList = data.Data;
      this.renderValueDuplicate = data.Data;
    });

  }
  showRecord() {
    this.renderValueList = this.renderValueDuplicate.slice(0, this.pageSize)

  }
}
