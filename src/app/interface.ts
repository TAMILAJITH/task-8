

export class DataCircle {
    Result1: customer[]
    Result2: contract[]

}

export class customer {
    CustomerID: string
    FirstName: string
    MiddleName: string
    LastName: string
}
export class contract {
    ContractNumber: string
}

export class Labour {
    EmployeeId: string
    Name: string
}

export class Ticket {
    Data: TicketType[]
}

export class TicketType {
    ID: number
    Name: string
}

export class department {
    ID: number
    Name: string
}

export class assignTo {
    Data: assignToValue[]
}
export class assignToValue {
    ID: number
    Name: string
}

export class DataPriority {
    Data: Priority[]
}
export class Priority {
    ID: number
    Name: string
}

export class DataCategory {
    TicketGroupID: number
    TicketGroupName: string
}

export class DataSubCategory {
    Id: number
    Description: string
}

export class DataRender {
    Data: RenderValue[]
}
export class RenderValue {
    CustomerName: string
    ContractNumber: string
    CustomerId: string
    TicketChannelName: string
    ContactNo: string
    Email: string
    TicketTypeName: string
    TicketAssignGroupName: string
    AssignedTo: string
    PriorityName: string
    GroupName: string
    SubGroupName: string
}

export class TicketList {
    Name: string
    Order: SortValue
}
export enum SortValue {
    Ascending = 1,
    Descending = 2
} 