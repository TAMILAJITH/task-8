import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataCircle, Labour, department, Ticket, assignTo, DataPriority, DataCategory, DataSubCategory, DataRender } from './interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private customerUrl: string = "https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150";
  private contractUrl: string = "https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150";



  constructor(private _http: HttpClient) { }

  customerValue(): Observable<DataCircle> {
    return this._http.get<DataCircle>(this.customerUrl);

  }
  contractValue(): Observable<DataCircle> {
    return this._http.get<DataCircle>(this.contractUrl)
  }

  labourValue(contractNumber): Observable<Labour[]> {

    const labourUrl: string = "https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=" + contractNumber;
    return this._http.get<Labour[]>(labourUrl)
  }

  ticketType(): Observable<Ticket> {
    const ticketTypeUrl: string = "https://erp.arco.sa:65//api/TickettypeList";
    return this._http.get<Ticket>(ticketTypeUrl);
  }

  department(ticketType): Observable<department[]> {
    const departmentUrl: string = "https://erp.arco.sa:65/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + ticketType;
    return this._http.get<department[]>(departmentUrl);
  }

  assignTo(): Observable<assignTo> {
    const assignToUrl: string = " https://erp.arco.sa:65//api/assigntoList?DepartmentId=";
    return this._http.get<assignTo>(assignToUrl);
  }

  priority(): Observable<DataPriority> {
    const priorityUrl: string = "https://erp.arco.sa:65//api/PriorityList";
    return this._http.get<DataPriority>(priorityUrl);
  }

  category(department): Observable<DataCategory[]> {
    const categoryUrl: string = "https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + department;
    return this._http.get<DataCategory[]>(categoryUrl)
  }

  subCategory(category): Observable<DataSubCategory[]> {
    const subCategoryUrl: string = " https://erp.arco.sa:65/api/SubGroupByGroup?id=" + category;
    return this._http.get<DataSubCategory[]>(subCategoryUrl);
  }

  renderGrid(): Observable<DataRender> {
    const renderGridUrl: string = "https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150";
    return this._http.get<DataRender>(renderGridUrl);
  }

}
