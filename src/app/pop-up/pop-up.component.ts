import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { contract, Labour, TicketType, customer, department, assignToValue, Priority, DataCategory, DataSubCategory } from '../interface';
import { FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {

  constructor(private _commonService: CommonService, private _form: FormBuilder) { }

  myArray: customer[] = [];
  secArray: contract[] = [];
  secArrayDuplicate: contract[] = [];
  labourID: Labour[] = [];
  ticketValue: TicketType[] = [];
  departmentValue: department[] = [];
  assignToValue: assignToValue[] = [];
  assignToDuplicate: assignToValue[] = [];
  priorityValue: Priority[] = [];
  categoryList: DataCategory[] = [];
  subCategoryList: DataSubCategory[] = [];

  get contractNumber() {
    return this.RegistrationForm.get('contractNumber');
  }

  get contact() {
    return this.RegistrationForm.get('contact');
  }


  get email() {
    return this.RegistrationForm.get('email');
  }

  get ticketChannel() {
    return this.RegistrationForm.get("ticketChannel");
  }

  get priority() {
    return this.RegistrationForm.get("priority");
  }

  get assignTo() {
    return this.RegistrationForm.get('assignTo');
  }

  RegistrationForm = this._form.group({
    customerName: [''],
    contractNumber: ['', Validators.required],
    labourID: [''],
    ticketChannel: ['', Validators.required],
    contact: ['', Validators.required],
    email: ['', Validators.required],
    ticketType: [''],
    department: [''],
    assignTo: [''],
    priority: ['', Validators.required],
    category: [''],
    subCategory: [''],
    subject: [''],
    description: [''],
    file: ['']

  });

  ngOnInit() {

    this._commonService.customerValue().subscribe(data => this.myArray = data.Result1);
    this._commonService.contractValue().subscribe(data => {
      this.secArray = data.Result2;
      this.secArrayDuplicate = data.Result2
    });

    this.RegistrationForm.get('contractNumber').valueChanges.subscribe((contractNumber) => {
      this._commonService.labourValue(contractNumber).subscribe(data => this.labourID = data)
    })

    this._commonService.ticketType().subscribe(data => this.ticketValue = data.Data);

    this.RegistrationForm.get('ticketType').valueChanges.subscribe((ticketType) => {
      this._commonService.department(ticketType).subscribe(
        (data) => this.departmentValue = data
      )
    });

    this.RegistrationForm.get('department').valueChanges.subscribe(() => {

      this._commonService.assignTo().subscribe(data => {

        this.assignToValue = data.Data
        this.assignToDuplicate = data.Data
      })
    })

    this._commonService.priority().subscribe(data => this.priorityValue = data.Data)

    this.RegistrationForm.get('department').valueChanges.subscribe((department) => {
      this._commonService.category(department).subscribe(data => this.categoryList = data)
    })
    this.RegistrationForm.get('category').valueChanges.subscribe((category) => {
      this._commonService.subCategory(category).subscribe(data => this.subCategoryList = data)
    })

  }

  onPress() {
    document.getElementById("showContractInput").style.display = "block";
    let store = this.RegistrationForm.get('contractNumber').value;
    this.secArray = this.secArrayDuplicate.filter(x => {
      return x.ContractNumber.toLocaleLowerCase().includes(store.toLowerCase())
    })
  }

  selectValue(parameterValue) {
    var store = parameterValue;
    this.contractNumber.setValue(store);
    document.getElementById("showContractInput").style.display = "none";
  }

  showAssignToValue() {

    document.getElementById('showInput').style.display = "block";
    let store = this.RegistrationForm.get('assignTo').value;
    this.assignToValue = this.assignToDuplicate.filter(function (x) {

      return x.Name.toLowerCase().includes(store.toLowerCase());


    })
  }

  onclick(item) {
    let dataFlow = item;
    this.assignTo.setValue(dataFlow);
    document.getElementById('showInput').style.display = "none";

  }

}
